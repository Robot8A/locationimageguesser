#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
File: run.py
Author: Ochoa Ortiz, Héctor
Last update: 2020-05-17
"""

import sys
import json
import random
import requests
import xmltodict
import urllib
import i18n
import os
from wikidata.client import Client
from functools import partial
from PyQt5.QtCore import QDateTime, Qt, QTimer, QUrl
from PyQt5.QtWidgets import QApplication, QWidget, QGridLayout, QVBoxLayout, QMessageBox, QPushButton, QSlider, QLabel, \
    QHBoxLayout, QComboBox
from PyQt5.QtGui import QIcon, QImage, QPixmap, QMouseEvent
from PyQt5.QtWebEngineWidgets import QWebEngineView
import bonuses
import locales

i18n.load_path.append('locales')
i18n.set('filename_format', '{locale}.{format}')
i18n.set('locale', 'en')
i18n.set('fallback', 'en')

config = {}
current_difficulty = 0

menuWidget = None
creditsWidget = None
playWidget = None

wdClient = Client()
commonsAPI = "https://magnus-toolserver.toolforge.org/commonsapi.php"
#rdmFileInCatAPI = "https://randomincategory.toolforge.org"
petScanAPI = "https://petscan.wmflabs.org/"
# https://petscan.wmflabs.org/?categories=Picos%20de%20Europa%0AMedia%20with%20locations&output_limit=1&project=wikimedia&language=commons&format=json&sortby=random&doit=Do it!&combination=subset

used_images = []


def show_infobox(text, info_text, win_title):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)

    msg.setText(text)
    msg.setInformativeText(info_text)
    msg.setWindowTitle(win_title)
    msg.setStandardButtons(QMessageBox.Ok)

    return msg.exec_()


def select_images(cat: str, n: int):
    ret = []
    selected = []
    first = True

    while len(selected) < n:
        # Get a random file in category
        if first:
            json_response = json.loads(requests.get(petScanAPI, params={
                "categories": cat + "\nMedia with locations",
                "depth": 2,
                "combination": "subset",
                "output_limit": 1,
                "project": "wikimedia",
                "language": "commons",
                "ns[6]": 1,  # namespace: File
                "format": "json",
                "sortby": "random",
                "add_coordinates": "on",
                "doit": "Do it!"
                }).text)["*"][0]["a"]["*"][0]
            file = json_response["title"]
            try:
                location = json_response["metadata"]["coordinates"].split("/")
                location = {"lat": location[0], "lon": location[1]}
                if file not in selected:
                    fr = requests.get(commonsAPI, params={"image": file, "thumbwidth": 350})
                    fr = fr.text
                    fr = xmltodict.parse(fr)['response']
                    ret.append({"author": fr['file']['author'], "location": location,
                                "url": fr['file']['urls']['thumbnail']})
                    selected.append(file)
                    first = False
            except Exception as e:
                print(file)
        else:
            json_response = json.loads(requests.get(petScanAPI, params={
                "categories": cat,
                "depth": 2,
                "combination": "subset",
                "output_limit": 1,
                "project": "wikimedia",
                "language": "commons",
                "ns[6]": 1,  # namespace: File
                "format": "json",
                "sortby": "random",
                "doit": "Do it!"
                }).text)["*"][0]["a"]["*"][0]
            file = json_response["title"]

            if file not in selected:

                try:
                    fr = requests.get(commonsAPI, params={"image": file, "thumbwidth": 350})
                    fr = fr.text
                    fr = xmltodict.parse(fr)['response']

                    ret.append({"author": fr['file']['author'], "url": fr['file']['urls']['thumbnail']})
                    selected.append(file)

                except Exception as e:
                    print(file)

    return ret


def get_difficulty_name():
    try:
        return config["difficulties"][current_difficulty]["name"][i18n.get('locale')]
    except KeyError:
        return config["difficulties"][current_difficulty]["name"][i18n.get('fallback')]


class Menu(QWidget):

    def __init__(self, parent: QWidget):
        super().__init__()

        self.setParent(parent)

        self.layout = QGridLayout(self)
        self.layout.setSpacing(10)

        self.logoImage = QLabel("")
        self.layout.addWidget(self.logoImage, 0, 0)
        self.logoImage.setPixmap(QPixmap.fromImage(QImage("logo.svg")))

        self.difficultySlider = QSlider(Qt.Horizontal, self)
        self.layout.addWidget(self.difficultySlider, 1, 0)
        self.difficultyText = QLabel(get_difficulty_name())
        self.layout.addWidget(self.difficultyText, 2, 0)
        self.difficultySlider.valueChanged.connect(self.changeDifficulty)
        self.difficultySlider.setValue(current_difficulty)
        self.difficultySlider.setMinimum(0)
        self.difficultySlider.setMaximum(len(config["difficulties"]) - 1)

        self.localeSelect = QComboBox()
        self.localeSelect.addItems(locales.available)
        self.localeSelect.setCurrentText(i18n.get('locale'))
        self.localeSelect.activated[str].connect(partial(self.localeAction))

        self.layout.addWidget(self.localeSelect, 3, 0)

        self.playBtn = QPushButton(i18n.t("play"))
        self.layout.addWidget(self.playBtn, 4, 0)
        self.playBtn.setFocusPolicy(Qt.StrongFocus)
        self.playBtn.clicked.connect(self.playAction)

        self.creditsBtn = QPushButton(i18n.t("credits"))
        self.layout.addWidget(self.creditsBtn, 5, 0)
        self.creditsBtn.clicked.connect(self.creditsAction)

        self.quitBtn = QPushButton(i18n.t("quit"))
        self.layout.addWidget(self.quitBtn, 6, 0)
        self.quitBtn.clicked.connect(QApplication.instance().quit)

        self.show()

    def changeDifficulty(self, value):
        global current_difficulty
        current_difficulty = value
        self.difficultyText.setText(get_difficulty_name())

    def playAction(self):
        global used_images, playWidget
        used_images = []
        playWidget = Play(self.parentWidget())
        self.close()

    def creditsAction(self):
        global creditsWidget
        creditsWidget = Credits(self.parentWidget())
        self.close()

    def localeAction(self, text):
        global menuWidget
        i18n.set('locale', text)
        menuWidget = Menu(self.parentWidget())
        self.close()


class Credits(QWidget):
    def __init__(self, parent: QWidget):
        super().__init__()

        self.setParent(parent)

        self.layout = QGridLayout(self)
        self.layout.setSpacing(10)

        self.testText = QLabel(i18n.t("creditsText"))
        self.layout.addWidget(self.testText, 0, 0)

        self.okBtn = QPushButton(i18n.t("ok"))
        self.layout.addWidget(self.okBtn, 1, 0)
        self.okBtn.clicked.connect(self.returnToMenu)

        self.show()

    def returnToMenu(self):
        global menuWidget
        menuWidget = Menu(self.parentWidget())
        self.close()


class Play(QWidget):
    def __init__(self, parent: QWidget):
        super().__init__()

        self.setParent(parent)

        self.status = {
            "round": 1,
            "timerStartValue": config["difficulties"][current_difficulty]["time"],
            "lives": config["difficulties"][current_difficulty]["lives"]
        }

        self.timer = 0

        self.layout = QVBoxLayout(self)

        self.menuLay = QHBoxLayout()

        self.difficultyText = QLabel(get_difficulty_name())
        self.difficultyText.setFixedWidth(50)
        self.difficultyText.setAlignment(Qt.AlignCenter)
        self.menuLay.addWidget(self.difficultyText)

        self.categoryText = QLabel()
        self.categoryText.setFixedWidth(160)
        self.categoryText.setAlignment(Qt.AlignCenter)
        self.menuLay.addWidget(self.categoryText)

        self.roundText = QLabel(i18n.t("round") + " " + str(self.status["round"]))
        self.roundText.setFixedWidth(50)
        self.roundText.setAlignment(Qt.AlignCenter)
        self.menuLay.addWidget(self.roundText)

        self.remainingLives = QLabel(" ❤ " + str(self.status["lives"]))
        self.menuLay.addWidget(self.remainingLives)

        self.remainingTime = QLabel("⏱ " + str(self.timer))
        self.menuLay.addWidget(self.remainingTime)

        self.quitBtn = QPushButton(i18n.t("quit"))
        self.quitBtn.clicked.connect(self.returnToMenu)
        self.menuLay.addWidget(self.quitBtn)

        self.layout.addLayout(self.menuLay)

        self.txtLay = QHBoxLayout()
        self.txtLay.addWidget(QLabel(i18n.t("chooseImg")))
        self.layout.addLayout(self.txtLay)

        self.location = None

        self.nI = config["difficulties"][current_difficulty]["imgNo"]

        self.imagLay = QHBoxLayout()
        self.images = []
        self.imgdata = []
        self.pixmaps = []
        self.imagAuthLay = QHBoxLayout()
        self.imgAuth = []
        for i in range(self.nI):
            self.images.append(QLabel(""))
            self.imgAuth.append(QLabel(""))
            self.imgdata.append(None)
            self.pixmaps.append(QPixmap("blank350.png"))
            self.images[i].setPixmap(self.pixmaps[i])
            self.imagLay.addWidget(self.images[i])
            self.imagAuthLay.addWidget(self.imgAuth[i])
        self.layout.addLayout(self.imagLay)
        self.layout.addLayout(self.imagAuthLay)

        self.mapLay = QHBoxLayout()
        self.map = QWebEngineView()
        self.map.setMinimumSize(300, 500)
        self.map.setUrl(QUrl("file:///" + os.path.abspath(os.path.join(os.path.dirname(__file__), "map/index.html"))))
        self.map.show()
        self.mapLay.addWidget(self.map)
        self.layout.addLayout(self.mapLay)

        self.show()

        if len(config["categories"]) > 1:
            self.category = None
            self.chooseCategory()
        else:
            self.category = config["categories"][0]
            self.updateCategoryText()
            self.roundStart()

    def updateCategoryText(self, catText: str = None):
        if catText is None:
            entity = wdClient.get(self.category)
            catText = str(entity.label)
        self.categoryText.setText(catText)

    def roundStart(self):
        entity = wdClient.get(self.category)
        self.selected_images = select_images(entity.attributes['claims']['P373'][0]['mainsnak']['datavalue']['value'],
                                             self.nI)
        random.shuffle(self.selected_images)
        for i in range(self.nI):
            self.imgdata[i] = urllib.request.urlopen(self.selected_images[i]["url"]).read()
            self.pixmaps[i].loadFromData(self.imgdata[i])
            self.images[i].setPixmap(self.pixmaps[i])
            self.imgAuth[i].setText(i18n.t("author") + " " + self.selected_images[i]["author"])
            try:
                # Correct image
                self.location = self.selected_images[i]['location']
                self.images[i].mousePressEvent = self.passedRound
                self.map.setUrl(QUrl("file:///" + os.path.abspath(os.path.join(
                    os.path.dirname(__file__), "map/index.html")) + "?lat=" + str(self.location["lat"]) + "&lon=" + str(
                    self.location["lon"])))
                self.map.show()
            except KeyError:
                self.images[i].mousePressEvent = self.failedRound

        self.timer = self.status["timerStartValue"]
        self.qtimer = QTimer(self)
        self.qtimer.timeout.connect(self.timerTick)  # Function called at every tick
        self.qtimer.start(1000)  # 1000 miliseconds == 1 second

        # Update texts that may have changed
        self.remainingTime.setText("⏱ " + str(self.timer))
        self.remainingLives.setText(" ❤ " + str(self.status["lives"]))

    def timerTick(self):
        self.timer -= 1
        self.remainingTime.setText("⏱ " + str(self.timer))
        if self.timer == 0:
            self.qtimer.stop()
            self.failedRound()

    def failedRound(self, event: QMouseEvent = None):
        if event is not None:
            event.accept()

        self.qtimer.stop()

        self.status["lives"] -= 1

        if self.status["lives"] == 0:
            show_infobox(
                i18n.t("youLost"),
                i18n.t("butYouMadeIt", round=str(self.status["round"])),
                i18n.t("end")
            )
            self.returnToMenu()
        else:
            show_infobox(
                i18n.t("failedRound"),
                i18n.t("stillAlive"),
                i18n.t("failedRoundTitle")
            )
            self.passedRound(None)

    def passedRound(self, event: QMouseEvent = None):
        if event is not None:
            event.accept()

            show_infobox(
                i18n.t("correct"),
                i18n.t("positiveMsg" + str(random.randrange(3))),
                i18n.t("passedRound")
            )

        self.qtimer.stop()

        self.status["round"] += 1
        self.roundText.setText(i18n.t("round") + " " + str(self.status["round"]))

        if self.status["round"] % config['bonusRounds'] == 0:
            # Define a new random bonus
            bonus = config["bonuses"][random.randrange(len(config["bonuses"]))]
            class_ = getattr(bonuses, bonus["name"])
            try:
                description = bonus["description"][i18n.get('locale')]
            except KeyError:
                description = bonus["description"][i18n.get('fallback')]
            instance: bonuses.Bonus = class_(bonus["name"], description)
            instance.apply(self.status)
            show_infobox(
                i18n.t("congratulations"),
                i18n.t("bonus", round=str(self.status["round"]), description=description),
                i18n.t("newBonus")
            )

        if self.status["round"] % config['catPickRounds'] == 0:
            self.chooseCategory()
        else:
            self.roundStart()

    def returnToMenu(self):
        global menuWidget
        menuWidget = Menu(self.parentWidget())
        self.close()

    def chooseCategory(self):
        if config["catPickNo"] == 1:
            return

        self.setVisible(False)

        categoriesPicked = []

        catWidget = QWidget(self.parent())
        catLayout = QVBoxLayout()
        catWidget.setLayout(catLayout)
        catLayout.addWidget(QLabel(i18n.t("chooseCat")))

        for _ in range(config["catPickNo"]):
            newCat = None
            while newCat is None or newCat in categoriesPicked:
                # Define a new random category
                newCat = config["categories"][random.randrange(len(config["categories"]))]
            categoriesPicked.append(newCat)

        def saveNewCat(cat, catText, catWidget, playWidget):
            self.category = cat
            catWidget.close()
            playWidget.setVisible(True)
            playWidget.updateCategoryText(i18n.t("cat") + " " + catText)
            playWidget.roundStart()

        for c in categoriesPicked:
            e = wdClient.get(c, load=True)
            try:
                label = str(e.label.texts[i18n.get('locale')])
            except KeyError:
                label = str(e.label)
            btn = QPushButton(label)
            btn.clicked.connect(partial(saveNewCat, c, label, catWidget, self))
            catLayout.addWidget(btn)

        catWidget.show()


class MainWindow(QWidget):

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Location Image Guesser")
        self.setWindowIcon(QIcon("logo.svg"))

        self.showMaximized()


def showCSCError(mW: MainWindow, error: str):
    widget = QWidget(mW)
    btn = QPushButton("&OK")
    layout = QVBoxLayout()
    widget.setLayout(layout)
    layout.addWidget(QLabel("ERROR:"))
    layout.addWidget(QLabel(error))
    layout.addWidget(QLabel("Please check config file"))
    layout.addWidget(btn)

    def exitWithError():
        QApplication.instance().exit(1)

    btn.clicked.connect(exitWithError)
    widget.show()
    sys.exit(app.exec_())


def configSanityChecks(mW: MainWindow):
    if len(config["categories"]) < 1:
        showCSCError(mW, "Categories must be greater than one")
    if config["catPickNo"] < 1:
        showCSCError(mW, "catPickNo must be greater than one")
    if config["catPickNo"] > len(config["categories"]):
        showCSCError(mW, "catPickNo must be smaller or equal than the number of categories")
    for c in config["categories"]:
        entity = wdClient.get(c)
        try:
            entity.attributes['claims']['P373']
        except AssertionError:
            showCSCError(mW, "entity " + c + " does not exist in Wikidata")
        except KeyError:
            showCSCError(mW, "entity " + c + " does not have a Commons Category")


if __name__ == "__main__":
    with open("config.json") as cfg_file:
        config = json.loads(cfg_file.read())

    # Set randomizer seed to current Unix time
    random.seed(QDateTime.currentDateTime().toSecsSinceEpoch())

    app = QApplication([])
    mW = MainWindow()

    configSanityChecks(mW)

    menuWidget = Menu(mW)

    sys.exit(app.exec_())
