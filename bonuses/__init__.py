# BONUSES #
# Create a new class inheriting the Bonus class and implement
# the apply function that changes the status dictionary

# --------------------------------
class Bonus:
    def __init__(self, name, description):
        self.name = name
        self.description = description

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def apply(self, status):
        pass
# --------------------------------


class Shield(Bonus):
    def apply(self, status):
        status["lives"] += 1


class ExtraTime(Bonus):
    def apply(self, status):
        status["timerStartValue"] += int(status["timerStartValue"]/10)
